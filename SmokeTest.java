package stepDefinition;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class SmokeTest {

	WebDriver driver;
	
	@Given("^open firefox and start application$")
	public void open_firefox_and_start_application() throws Throwable {
		System.setProperty("webdriver.gecko.driver", "C:\\Program Files\\gecko\\geckodriver.exe");
		
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://ecourse.del.ac.id/login/index.php");

	}

	@When("^I enter valid username and valid password$")
	public void I_enter_valid_username_and_valid_password() throws Throwable {
		driver.findElement(By.id("username")).sendKeys("if317008");
		driver.findElement(By.id("password")).sendKeys("sweta12345678");

	}

	@Then("^I can login successfully$")
	public void I_can_login_successfully() throws Throwable {
		driver.findElement(By.id("loginbtn")).click();
	}
	
	
	//Fungsi search keyword course
	@When("^I enter key word$")
	public void I_enter_key_word() throws Throwable {
		driver.findElement(By.id("coursesearchbox")).click();
	}



//	@Then("^I find result$")
//	public void I_find_result() throws Throwable {
//	driver.findElement(By.button("coursesearchbox")).click();
//}

}

