package automationFramework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

//import org.openqa.selenium.chrome.ChromeDriver;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirstTestCase {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.gecko.driver", "C:\\Program Files\\gecko\\geckodriver.exe");
		WebDriver wd = new FirefoxDriver();
//		wd.findElement(By.id("UserName")).clear();
//		wd.findElement(By.id("UserName")).sendKeys("ToolsSQA");
//		wd.findElement(By.id("https:/cis.del.ac.id")).submit();
		
		wd.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//Launch the Online Store Website		
		wd.get("https:/cis.del.ac.id");
		
		wd.findElement(By.name("username")).sendKeys("https://cis.del.ac.id");
		
//		wd.get(appUrl);
		
//		wd.findElement(By.xpath(".//*[@id='menu-item-374']/a")).click();
		
		String title = wd.getTitle();
		int titlelength = wd.getTitle().length();
		
		
		System.out.println("Title the page is: "+title);
		System.out.println("Title the page is: "+titlelength);
		
		String pageSource = wd.getPageSource();
		int pageSourceLength = pageSource.length();
		
		System.out.println("Total length of the Page Source is: "+pageSourceLength);
		
		wd.getCurrentUrl();
		
		
		wd.navigate().back();
		
		wd.navigate().forward();

		wd.navigate().refresh();
		

	
//		wd.getPageSource();
		
		
		//Print message to the screen
		System.out.println("Successfully opened the website Online CIS DEL");
		
		//Wait for 5 sec
		Thread.sleep(5000);
		

		//Close the driver
		wd.quit();
		
		
	}

}
